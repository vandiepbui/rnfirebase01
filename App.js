import React, { Component } from "react";
import { AppState, StyleSheet, Text, View } from "react-native";

import PushNotification from "./components/PushNotification.js";

export default class App extends Component {
  state = {
    appState: AppState.currentState
  };

  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    console.log(nextAppState);

    this.setState({ appState: nextAppState });
  };

  render() {
    return (
      <View style={styles.container}>
        <PushNotification />
        <Text style={styles.text}>{"appState: " + this.state.appState}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  text: {
    flex: 1,
    textAlign: "center",
    color: "#333333",
    marginTop: 10
  }
});
