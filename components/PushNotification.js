import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Alert } from "react-native";

import AsyncStorage from "@react-native-community/async-storage";

import firebase from "react-native-firebase";

export default class PushNotification extends Component {
  state = {
    name: "",
    email: ""
  };
  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners();
  }

  componentWillUnmount() {
    this.notificationListener;
    this.notificationOpenedListener;
  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      // user has permissions
      this.getToken();
    } else {
      // user doesn't have permission
      this.requestPermission();
    }
  }

  async createNotificationListeners() {
    //trigger when a particular notification has been received in foreground
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        const localNotification = new firebase.notifications.Notification({
          sound: "audio",
          show_in_foreground: true
        })
          .setNotificationId(notification.notificationId)
          .setTitle(notification.title)
          //.setSubtitle(notification.subtitle)
          .setBody(notification.body)
          .setData(notification.data)
          .android.setChannelId("fcm_default_channel")
          .android.setSmallIcon("@drawable/ic_launcher") // create this icon in Android Studio
          .android.setColor("red") // you can set a color here
          .android.setAutoCancel(true)
          .android.setPriority(firebase.notifications.Android.Priority.High);

        firebase
          .notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));
      });

    const channel = new firebase.notifications.Android.Channel(
      "fcm_default_channel",
      "Demo app name",
      firebase.notifications.Android.Importance.High
    )
      .setDescription("Demo app description")
      .setSound("audio.mp3");
    firebase.notifications().android.createChannel(channel);

    //if app in  foreground or background, you can listen for when a notification is clicked / tapped / opened as follow:
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        // Get information about the notification that was opened
        const notification = notificationOpen.notification;

        this.setState({
          name: notification.data.name,
          email: notification.data.email
        });

        firebase
          .notifications()
          .removeDeliveredNotification(notification.notificationId);
      });

    //if app is closed, you can check if it was opened by notification being clicked / tapped / opened as follow:
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      // Get information about the notification that was opened
      const notification = notificationOpen.notification;
      this.setState({
        name: notification.data.name,
        email: notification.data.email
      });
    }

    //triggered for data only payload in foreground
    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
      console.log(JSON.stringify(message));
    });
  }

  //3
  async getToken() {
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      // user has a device token
      // console.log("fcmToken: ", fcmToken);
      await AsyncStorage.setItem("fcmToken", fcmToken);
    } else {
      // user doesn't have a device token yet
      fcmToken = await firebase.messaging().getToken();
    }
    console.log("fcmToken:", fcmToken);
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      //User has rejected permissions
      console.log("Permission rejected");
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>{"Username: " + this.state.name}</Text>
        <Text style={styles.welcome}>{"Email: " + this.state.email}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
